let laData;

let datosUsuario = {
    ocultarCursosSolapan: true,
    escribirCursosSeleccionados: true
};

let ajax = new XMLHttpRequest();

let miHorario = {};

ajax.onreadystatechange = () => {
    if (ajax.status === 200 && ajax.readyState === 4) {
        laData = JSON.parse(ajax.responseText);

        crearTablas(0, document.getElementById("Horario0anio"));
        crearTablas(1, document.getElementById("Horario1anio"));
        crearTablas(2, document.getElementById("Horario2anio"));
        crearTablas(3, document.getElementById("Horario3anio"));
        crearTablas(4, document.getElementById("Horario4anio"));
        crearTablas(5, document.getElementById("Horario5anio"));

        ocultarTabla(0);
        /*ocultarTabla(1);*/
        ocultarTabla(2);
        ocultarTabla(3);
        ocultarTabla(4);
        ocultarTabla(5);

        crearDatos(laData, false);
    }
};

ajax.open("GET", "./data.json", true);
ajax.send();

function crearDatos(data, nombreCompleto, ...limpiar) {

    if (data === undefined) {
        data = laData;
    }

    if (limpiar !== undefined) {
        for (const a in limpiar) {
            limpiarTabla(a);
            limpiarDescripciones(a);
        }
    }

    for (let anio in data) {
        let anioData = data[anio];

        for (let curso in anioData) {
            let cursoData = anioData[curso];

            if (cursoData["oculto"] !== undefined && cursoData["oculto"] === true) continue;

            let cursoDataDiv = document.createElement("div");
            cursoDataDiv.className = "cursoDataDiv descrCurso--Light--On";
            cursoDataDiv.setAttribute("req","");

            let nombreCurso = document.createElement("span");
            nombreCurso.style.fontWeight = "bold";
            nombreCurso.appendChild(document.createTextNode(curso.substring(0, curso.search("->")) + " - " + curso.substring(curso.search("->") + 2) + " "));
            cursoDataDiv.appendChild(nombreCurso);

            let input = document.createElement("input");
            input.type = "checkbox";
            input.checked = "checked";
            input.addEventListener("click", () => {
                ocultarCurso(input.checked, cursoDataDiv, (curso.substring(curso.search("->") + 2)), anio)
            });
            cursoDataDiv.appendChild(input);


            for (let variante in cursoData) {

                if (variante === "oculto") continue;

                cursoDataDiv.appendChild(document.createElement("br"));
                let span = document.createElement("span");
                span.innerText = variante + " = ";
                cursoDataDiv.appendChild(span);

                /* Crea temporalmente un objeto para manejar los Grupos y Profesores */
                let gruposYProf = {};

                let varianteData = cursoData[variante];
                for (let grupo in varianteData) {

                    let grupoData = varianteData[grupo];

                    /* Inicializa el objeto */
                    if (gruposYProf[grupoData["Docente"]] === undefined) {
                        gruposYProf[grupoData["Docente"]] = grupo.substring(grupo.indexOf(" ") + 1);
                    } else {
                        gruposYProf[grupoData["Docente"]] += "," + grupo.substring(grupo.indexOf(" ") + 1);
                    }

                    /* NUEVO ESTANDAR PARA LOS DIV:
                     * En vez de que el DIV tenga ID 1er_anioLu0700
                      * se reemplazará por Lu07001er_anio
                      * basicamente para evitar problemas con el DOM.*/
                    for (let horaIndex in grupoData["Horas"]) {
                        let hora = grupoData["Horas"][horaIndex];
                        let elDiv = document.getElementById(hora + anio);

                        /* <codigo><variante><grupo><anio> -> C1VTeoriaA1er_anio */
                        let className = (curso.substring(curso.search("->") + 2)) + variante + (grupo.substring(grupo.indexOf(" ") + 1)) + anio;

                        /* <codigo><anio> -> C1V1er_anio */
                        let classGenerica = (curso.substring(curso.search("->") + 2)) + anio;
                        cursoDataDiv.id = `descr${classGenerica}`;

                        /* <codigo><variante><anio> -> C1V1er_anio */
                        let classNV = (curso.substring(curso.search("->") + 2)) + variante + anio;

                        let laData = document.createElement("div");
                        laData.className = "nombreCursoTeoriaLight dropdown " + className + " " + classGenerica + " " + classNV;
                        laData.addEventListener("mouseover", () => {
                            cambiarBack(className, true)
                        });
                        laData.addEventListener("mouseout", () => {
                            cambiarBack(className, false)
                        });

                        let laInnerData = document.createElement("div");
                        laInnerData.className = "dropdown-content";
                        let nombreCurso = document.createElement("span");
                        nombreCurso.style.fontWeight = "bold";
                        laInnerData.appendChild(nombreCurso);
                        laInnerData.appendChild(document.createElement("hr"));

                        if (variante === "Laboratorio") {
                            cursoDataDiv.setAttribute("req",cursoDataDiv.getAttribute("req") + "L");

                            if (nombreCompleto) {
                                let labI = document.createElement("span");
                                laData.className = laData.className.replace(/Teoria/,"Lab");
                                labI.appendChild(document.createTextNode(curso.substring(0, curso.search("->")) +
                                    grupo.substring(grupo.search(" ")) + " (L)"));
                                laData.appendChild(labI);

                                nombreCurso.innerText = curso.substring(0, curso.search("->"));

                            } else {
                                let labI = document.createElement("span");
                                laData.className = laData.className.replace(/Teoria/,"Lab");
                                labI.appendChild(document.createTextNode(curso.substring(curso.search("->") + 2) +
                                    grupo.substring(grupo.search(" ")) + " (L)"));
                                laData.appendChild(labI);

                                nombreCurso.innerText = curso.substring(0, curso.search("->"));

                            }
                            laInnerData.appendChild(document.createTextNode(grupo + " (Lab)"));
                        }
                        else {
                            cursoDataDiv.setAttribute("req",cursoDataDiv.getAttribute("req") + "T");

                            if (nombreCompleto) {

                                laData.appendChild(document.createTextNode(curso.substring(0, curso.search("->")) +
                                    grupo.substring(grupo.search(" "))));

                                nombreCurso.innerText = curso.substring(0, curso.search("->"));

                            } else {

                                laData.appendChild(document.createTextNode(curso.substring(curso.search("->") + 2) +
                                    grupo.substring(grupo.search(" "))));

                                nombreCurso.innerText = curso.substring(0, curso.search("->"));

                            }
                            laInnerData.appendChild(document.createTextNode(grupo));
                        }

                        laInnerData.appendChild(document.createElement("br"));
                        laInnerData.appendChild(document.createTextNode(grupoData["Docente"]));

                        if (anio.substring(0, 1) !== "0") {
                            laInnerData.appendChild(document.createElement("hr"));
                            let linkParaAnadir = document.createElement("span");
                            linkParaAnadir.style.textDecoration = "underline";
                            linkParaAnadir.style.color = "blue";
                            linkParaAnadir.style.cursor = "pointer";
                            linkParaAnadir.appendChild(document.createTextNode("Añadir a mi Horario"));
                            linkParaAnadir.addEventListener("click", () => {
                                anadirAMiHorario(anio, curso, variante, grupo)
                            });
                            laInnerData.appendChild(linkParaAnadir);
                        }

                        laData.appendChild(laInnerData);

                        let texto = document.createTextNode(" | ");

                        try {
                            elDiv.appendChild(laData);
                            elDiv.appendChild(texto);
                        } catch (e) {
                            console.log("El Div " + hora + anio + " no existe | " + curso + " " + variante + " "
                                + grupo + "\n\n" + e.stack);
                        }

                    }
                }

                for (let prof in gruposYProf) {
                    let data = gruposYProf[prof];
                    let datos = " (" + data + ") ";

                    let span2 = document.createElement("span");
                    span2.innerText = datos + prof + ",";

                    span.appendChild(span2);
                }

            }

            document.getElementById("Horario" + anio.substring(0, 1) + "anio").appendChild(cursoDataDiv);
        }

    }

    //modoNocturno();
}

function limpiarTabla(num) {
    let rows = document.querySelector("#tablaHorario" + num + "anio").rows;

    for (let row of rows) {
        for (let cell of row.cells) {
            if (cell.id !== "") {
                cell.innerHTML = "";
            }
        }
    }

}

function limpiarDescripciones(num) {
    let mayorDiv = document.getElementById("Horario" + num + "anio");
    let valores = mayorDiv.children;
    for (let a = 0; a < valores.length; a++) {
        let node = valores[a];
        if (/cursoDataDiv/.test(node.className)) {
            mayorDiv.removeChild(node);
            a--;
        }
    }
}

function ocultarTabla(num) {
    let section = document.getElementById("Horario" + num + "anio");
    let isCollapsed = section.getAttribute('data-collapsed') === 'true';

    if (isCollapsed) {
        expandSection(section);
        section.setAttribute('data-collapsed', 'false');
    } else {
        collapseSection(section);
    }
}

function cambiarBack(nombreClase, paraAplicar) {
    let data = document.querySelectorAll("." + nombreClase);
    if (paraAplicar) {
        for (let a = 0; a < data.length; a++) {
            data[a].style.color = "black";
            let temp = data[a].parentNode;
            chechAttribute(temp, false, 0);
        }
    } else {
        for (let a = 0; a < data.length; a++) {
            data[a].style = "";
            let temp = data[a].parentNode;
            chechAttribute(temp, false, -1, true)
        }
    }
}

function collapseSection(element) {
    /* get the height of the element's inner content, regardless of its actual size*/
    let sectionHeight = element.scrollHeight;

    /* temporarily disable all css transitions*/
    let elementTransition = element.style.transition;
    element.style.transition = '';

    /* on the next frame (as soon as the previous style change has taken effect),
    // explicitly set the element's height to its current pixel height, so we
    // aren't transitioning out of 'auto'*/
    requestAnimationFrame(function () {
        element.style.height = sectionHeight + 'px';
        element.style.transition = elementTransition;

        /* on the next frame (as soon as the previous style change has taken effect),
         have the element transition to height: 0*/
        requestAnimationFrame(function () {
            element.style.height = 0 + 'px';
        });
    });

    /* mark the section as "currently collapsed"*/
    element.setAttribute('data-collapsed', 'true');
}

function expandSection(element) {
    /* get the height of the element's inner content, regardless of its actual size*/
    let sectionHeight = element.scrollHeight;

    /* have the element transition to the height of its inner content*/
    element.style.height = sectionHeight + 'px';

    /* when the next css transition finishes (which should be the one we just triggered)*/
    element.addEventListener('transitionend', function (e) {
        // remove this event listener so it only gets triggered once
        element.removeEventListener('transitionend', arguments.callee);

        /* remove "height" from the element's inline styles, so it can return to its initial value*/
        element.style.height = null;
    });

    /* mark the section as "currently not collapsed"*/
    element.setAttribute('data-collapsed', 'false');
}

function anadirAMiHorario(anio, curso, variante, grupo) {

    const idCurso = "descr" + (curso.substring(curso.search("->") + 2)) + anio;

    /* Si aun no se creo el horario */
    if (miHorario["0_anio"] === undefined) {
        miHorario["0_anio"] = {};
    }

    /* Si el curso a añadir no existe */
    if (miHorario["0_anio"][curso] === undefined) {
        miHorario["0_anio"][curso] = JSON.parse("{\"" + variante + "\" : { \"" + grupo + "\" : {}}}");
        miHorario["0_anio"][curso][variante][grupo] = laData[anio][curso][variante][grupo];
        reservarCeldas(variante, idCurso, ...(miHorario["0_anio"][curso][variante][grupo]["Horas"]));
    }
    /* El curso a añadir si existe */
    else {
        /* El curso a añadir existe, pero no la variante(Teoria/Lab) */
        if (miHorario["0_anio"][curso][variante] === undefined) {
            miHorario["0_anio"][curso][variante] = JSON.parse("{ \"" + grupo + "\" : {}}");
            miHorario["0_anio"][curso][variante][grupo] = laData[anio][curso][variante][grupo];
            reservarCeldas(variante, idCurso, ...(miHorario["0_anio"][curso][variante][grupo]["Horas"]));
        }
        /* El curso y la variante existen. Sospechoso...*/
        else {
            /* El curso y la variante existen, pero no el grupo. ¿Quieres añadir otro grupo? Que extraño...*/
            if (miHorario["0_anio"][curso][variante][grupo] === undefined && confirm("¿Quieres añadir otro grupo de este curso?")) {
                miHorario["0_anio"][curso][variante][grupo] = laData[anio][curso][variante][grupo];
                reservarCeldas(undefined, undefined, ...(miHorario["0_anio"][curso][variante][grupo]["Horas"]));
            }
            /*El curso, la variante y el grupo existen. ¿Quieres añadir un curso duplicado? Nope*/
            else {
                alert("Este curso ya existe.");
            }
        }
    }
    crearDatos(miHorario, false, 0);
}

function reservarCeldas(variante, idCurso, ...id) {
    if (variante !== undefined && idCurso !== undefined) {
        console.log(idCurso);
        const elem = document.getElementById(idCurso);
        elem.setAttribute(variante,"true");
        let condicion = false;
        if (/T/.test(elem.getAttribute("req"))){
            if (/L/.test(elem.getAttribute("req"))){
                condicion = elem.getAttribute("Teoria") === "true" && elem.getAttribute("Laboratorio") === "true";
            } else {
                condicion = elem.getAttribute("Teoria") === "true";
            }
        } else if (/L/.test(elem.getAttribute("req"))){
            condicion = elem.getAttribute("Laboratorio") === "true";
        }

        if (condicion){
            elem.className = elem.className.replace(/On/,"OK");
            elem.className = elem.className.replace(/Off/,"OK");
        }
    }

    for (let index in id) {
        let valor = id[index];
        let divs = document.querySelectorAll("." + valor);
        let div;

        for (let i = 0; i < divs.length, div = divs[i]; i++) {
            chechAttribute(div, true);
        }
    }
}

function chechAttribute(div, set = false, offset = 0, isOnMouseUp = false) {
    const COLORES = ["#ABE77E", "#ffcb6b", "#b74d33", "#ff002f", "#454545", "#454545"];
    if (div.getAttribute("hora-ocupada") === "1") {
        if (set)
            div.setAttribute("hora-ocupada", "2");
        div.style.backgroundColor = COLORES[1 + offset];
    } else if (div.getAttribute("hora-ocupada") === "2") {
        if (set)
            div.setAttribute("hora-ocupada", "3");
        div.style.backgroundColor = COLORES[2 + offset];
    } else if (div.getAttribute("hora-ocupada") === "3") {
        if (set)
            div.setAttribute("hora-ocupada", "4");
        div.style.backgroundColor = COLORES[3 + offset];
    } else if (!isNaN(parseInt(div.getAttribute("hora-ocupada"))) && parseInt(div.getAttribute("hora-ocupada")) > 3) {
        if (set)
            div.setAttribute("hora-ocupada", "5");
        div.style.backgroundColor = COLORES[4 + offset];
    } else {
        if (set)
            div.setAttribute("hora-ocupada", "1");

        if (isOnMouseUp)
            div.style.backgroundColor = "transparent";
        else
            div.style.backgroundColor = COLORES[0];

    }
}

function crearTablas(anio, divDestino,
                     indiceArray = ["Hora", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes"],
                     patronHoras = ["07:00", "07:50", ["08:40"], "08:50", "09:40", ["10:30"], "10:40",
                         "11:30", "12:20", "13:10", "14:00", "14:50", ["15:40"], "15:50",
                         "16:40", ["17:30"], "17:40", "18:30", "19:20"]) {
    const nomenclaturaAnios = ["0_anio", "1er_anio", "2do_anio", "3er_anio", "4to_anio", "5to_anio"];
    const nombreAnio = nomenclaturaAnios[anio];

    let tabla = document.createElement("table");
    tabla.id = "tablaHorario" + anio + "anio";
    tabla.className = "striped";

    let thead = document.createElement("thead");
    let tr = document.createElement("tr");
    for (let index in indiceArray) {
        let valor = indiceArray[index];
        let td = document.createElement("td");
        td.innerText = valor;
        tr.appendChild(td);
    }
    thead.appendChild(tr);
    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");
    for (let index in patronHoras) {
        let valor = patronHoras[index];
        let tr = document.createElement("tr");
        let isHoraDescanso = false;
        if (typeof valor === "object") {
            valor = valor[0];
            tr.className = "trDescansoLight";
            isHoraDescanso = true;
        }

        for (let i in indiceArray) {
            let td = document.createElement("td");
            if (i === "0") {
                td.innerText = valor;
            } else if (!isHoraDescanso) {
                td.id = indiceArray[i].substring(0, 2) + valor.replace(":", "") + nombreAnio;
                td.className = indiceArray[i].substring(0, 2) + valor.replace(":", "");
            }
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
    tabla.appendChild(tbody);

    divDestino.appendChild(tabla);
}

function ocultarCurso(ocultar, div, curso, anio) {
    if (ocultar) {

        div.className = div.className.replace(/Off/,"On");

        let lista = document.querySelectorAll("." + curso + anio);
        let iDiv;
        for (let i = 0; i < lista.length, iDiv = lista[i]; i++) {
            iDiv.style.display = "inline-block";
        }
    } else {

        div.className = div.className.replace(/On/,"Off");

        let lista = document.querySelectorAll("." + curso + anio);
        console.log("Query: " + "." + curso + anio);
        let iDiv;
        for (let i = 0; i < lista.length; i++) {
            iDiv = lista[i];
            iDiv.style.display = "none";
        }
    }
}

function ocultarVariante(ocultar, div, anio, curso, variante) {

}

function ocultarGrupo(ocultar, div, anio, curso, variante, grupo) {

}