let fs = require('fs');

const tabla = {};

fs.readFile('./raw3anioData.json',(err, data) => {
    const miData = JSON.parse(data);
    crearData(miData);
});


function crearData(data) {
    let datos = data["Hoja1"];

    let indiceAnios = [];
    let indiceDatos = [undefined,undefined];
    let cursosMap = {};
    let anioActual = -1;
    /* datos contiene un Array con las filas, cada una de estas filas es un array con las posiciones. */

    const DIAS = ["Lunes","Martes","Miercoles","Jueves","Viernes"];
    const NOMBRES_ANIOS = "(primer | segundo | tercer | cuarto | quinto)";
    const regexNombresAnios = new RegExp(NOMBRES_ANIOS,"i");
    const regexHoras = /\d\d\s*[:]\s*\d\d\s*[-]\s*\d\d\s*[:]\s*\d\d/;
    const regexCursos = /[A-Z]+\s*[=]\s*\w+/;

    for (let index in datos) {
        let fila = datos[index];

        for (let indexF in fila) {
            let celda = fila[indexF];

            let horaActual = "";
            let diaActual = 0;

            /* Revisa si es un inicio de anio */
            if ((/semestre/i).test(celda) === true) {
                console.log("Celda semestre: " + celda);
                let nombreAnio = (regexNombresAnios.exec(celda))[0].trim();
                console.log("Nombre del Anio: '" + nombreAnio + "'");
                anioActual++;
                indiceAnios.push(nombreAnio);
                tabla[nombreAnio] = {};
            }
            /* Encuentra la de los cursos */
            else if (regexHoras.test(celda)) {
                console.log(`Hora actual: ${regexHoras.exec(celda)}`);
            }
            /* Encuentra los cursos en si */
            else if (regexCursos.test(celda)) {
                console.log("Encontre una definicion de curso.");
            }
        }
    }

    console.log(`Al final el obj es:\n\n${JSON.stringify(tabla)}\n\n`);
}





